<?php
/*
 * The MIT License
 *
 * Copyright 2021 diemarc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace App;

/**
 * Description of Calculator
 *
 * @author diemarc
 */
class Calculator
{
    protected array
    $numbers;
    public int|float $num1;

    public function __construct()
    {

    }

    /**
     * Seta un array de numeros
     * @param array $numbers
     * @return void
     */
    public function setNumbers(array $numbers): void
    {

        if (empty($numbers)) {

            throw new \InvalidArgumentException('numbers esta vacio');
        }

        $this->numbers = $numbers;
    }


    
    /**
     * Suma todos los numeros
     * @return type
     */
    public function plus()
    {

        if (empty($this->numbers)) {
            Throw new \InvalidArgumentException('numbers esta vacio');
        }

        return array_sum($this->numbers);
    }
}