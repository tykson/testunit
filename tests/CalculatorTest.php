<?php
/*
 * The MIT License
 *
 * Copyright 2021 diemarc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Calculator;

/**
 * Description of CalculatorTest
 *
 * @author diemarc
 */
class CalculatorTest extends TestCase
{
    private $Calculator;

    public function setUp(): void
    {
        $this->Calculator = new Calculator();
    }

    /**
     * Comprueba si devuelve una exepcion al pasar un parametro que no es un array
     */
    public function testSetNumbersIsArray()
    {

        $this->expectException(\InvalidArgumentException::class);
        $this->Calculator->setNumbers([]);
    }

    /**
     * Comprueba si la suma falla si el array de numeros son vacios
     */
    public function testPlusIfEmptyNumbers()
    {

        $this->expectException(\InvalidArgumentException::class);
        $this->Calculator->plus();
    }

    /**
     * comprueba si suma bien, es decir el resultado
     */
    public function testPlusResult()
    {
        $this->Calculator->setNumbers([2,3,4]);
        $this->assertEquals(9, $this->Calculator->plus());
    }
}